let user = JSON.parse(sessionStorage.user || null);
let refund_headline = document.querySelector('.refund-headline');
let refund_Btn = document.querySelector('.refund-btn');

const sendData = (path, data) => {
    fetch(path, {
            method: 'post',
            headers: new Headers({ 'Content-Type': 'application/json' }),
            body: JSON.stringify(data)
        }).then((res) => res.json())
        .then(response => {
            processData(response);
        })
}

const processData = (data) => {
    if (data.alert) {
        if (data.type) {
            showAlert(data.alert, 'success');
        } else {
            showAlert(data.alert);
        }
    } else if (data.name) {
        // create authToken
        data.authToken = generateToken(data.email);
        sessionStorage.user = JSON.stringify(data);
        location.replace('/');
    } else if (data == true) {
        // seller page
        let user = JSON.parse(sessionStorage.user);
        user.seller = true;
        sessionStorage.user = JSON.stringify(user);
        location.reload();
    } else if (data.product) {
        location.href = '/seller';
    } else if (data == 'review') {
        alert('got the review');
        location.reload();
    }
}

refund_Btn.addEventListener('click', () => {
    if (!refund_headline.value.length) {

        showFormError('Fill the input');

    } else {
        sendData('/approve-refund-req', {
            orderID: refund_headline.value,

        })

    }

})

const showFormError = (err) => {
    let errorEle = document.querySelector('.error');
    errorEle.innerHTML = err;
    errorEle.classList.add('show')

    setTimeout(() => {
        errorEle.classList.remove('show')
    }, 2000)
}

//fetch review

const getRefundReq = () => {

    fetch('/get-refund-req', {
            method: 'post',
            headers: new Headers({ 'Content-Type': 'application/json' }),
            body: JSON.stringify({
                email: "admin",
            })
        })
        .then(res => res.json())
        .then(data => {
            if (data.length) {
                createRefundReqSection(data)
            }
        })
}

const createRefundReqSection = (data) => {
    let section = document.querySelector('.refund-section');

    section.innerHTML += `
        <h1 class="section-title">Refunds</h1>
        <div class="review-container">
            ${createRefundReqCard(data)}
            
        </div>
    `
}



const createRefundReqCard = data => {
    let cards = '';

    for (let i = 0; i < 50; i++) {
        if (data[i]) {



            cards += `
            <div class="review-card">
                <h2 class="review-title">Order ID: ${(data[i].orderID)} </h2>

                <h2 class="review-title">Email: ${(data[i].email)} </h2>
                <p class="review">Product Name: ${(data[i].productName)} </p>
                <p class="review">Product Name: ${(data[i].orderPrice)} </p>

                <br>

                <hr style="border-top: dotted 1px;" />
                <br>
            </div>
            `
        }
    }



    return cards;
}



getRefundReq();