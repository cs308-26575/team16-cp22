//fetch review

const getReviews2 = () => {

    fetch('/get-reviews2', {
            method: 'post',
            headers: new Headers({ 'Content-Type': 'application/json' }),
            body: JSON.stringify({
                email: "admin",
            })
        })
        .then(res => res.json())
        .then(data => {
            if (data.length) {
                createReviewSection2(data)
            }
        })
}

const createReviewSection2 = (data) => {
    let section = document.querySelector('.review-section');

    section.innerHTML += `
        <h1 class="section-title">Reviews</h1>
        <div class="review-container">
            ${createReviewCard2(data)}
            
        </div>
    `
}



const createReviewCard2 = data => {
    let cards = '';

    for (let i = 0; i < 50; i++) {
        if (data[i]) {
            cards += `
            <div class="review-card">
                
                <div class="user-dp" data-rating="${data[i].rate}"></div>
                <h2 class="review-title">${data[i].headline}</h2>
                <h2 class="review-title">${data[i].rate}</h2>
                <p class="review">${data[i].review}</p>
                <br>
                <button class="approveBtn">Approve</button>
                <button class="declineBtn">Decline</button>
                <hr style="border-top: dotted 1px;" />
                <br>
            </div>
            `
        }
    }



    return cards;
}



getReviews2();