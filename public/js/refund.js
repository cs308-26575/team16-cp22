let user = JSON.parse(sessionStorage.user || null);
let refund_headline = document.querySelector('.refund-headline');
let refund_Btn = document.querySelector('.refund-btn');

const sendData = (path, data) => {
    fetch(path, {
            method: 'post',
            headers: new Headers({ 'Content-Type': 'application/json' }),
            body: JSON.stringify(data)
        }).then((res) => res.json())
        .then(response => {
            processData(response);
        })
}

const processData = (data) => {
    if (data.alert) {
        if (data.type) {
            showAlert(data.alert, 'success');
        } else {
            showAlert(data.alert);
        }
    } else if (data.name) {
        // create authToken
        data.authToken = generateToken(data.email);
        sessionStorage.user = JSON.stringify(data);
        location.replace('/');
    } else if (data == true) {
        // seller page
        let user = JSON.parse(sessionStorage.user);
        user.seller = true;
        sessionStorage.user = JSON.stringify(user);
        location.reload();
    } else if (data.product) {
        location.href = '/seller';
    } else if (data == 'review') {
        alert('got the review');
        location.reload();
    }
}

refund_Btn.addEventListener('click', () => {
    if (!refund_headline.value.length) {
        showFormError('Fill the input');
    } else {


        //db.collection('order').doc(refund_headline.value).get({ orderID: doc.id })
        // send the data to backend
        sendData('/add-refund-request', {
            orderID: refund_headline.value,
            email: user.email,
            productName: "",
            orderPrice: "",
            approvalRefundRequest: true,
            approvedOrDisapproved: "disapproved"
        })

    }

})

const showFormError = (err) => {
    let errorEle = document.querySelector('.error');
    errorEle.innerHTML = err;
    errorEle.classList.add('show')

    setTimeout(() => {
        errorEle.classList.remove('show')
    }, 2000)
}

//fetch review



const getRefunds = () => {

    fetch('/get-refunds', {
            method: 'post',
            headers: new Headers({ 'Content-Type': 'application/json' }),
            body: JSON.stringify({
                email: user.email,
            })
        })
        .then(res => res.json())
        .then(data => {
            createRefundSection(data)
        })
}

const createRefundSection = (data) => {
    let section = document.querySelector('.refund-section');

    section.innerHTML += `
        <h1 class="section-title">My Orders</h1>
        <hr style="border-top: dotted 1px;" /><br>
        <div class="review-container">
            ${createRefund(data)}
            
        </div>
    `
}



const createRefund = data => {
    let cards = '';

    for (let i = 0; i < 50; i++) {
        if (data[i]) {
            cards += `
            <div class="review-card">
                <h2 class="review-title">Order ID: ${(data[i].orderID)} </h2><br>
                <p class="review">Product Name: ${(data[i].order)[0].name}$ </p>

                <p class="review">Price: ${(data[i].order)[0].sellPrice}$ </p>
                <p class="review">Address: ${(data[i].add).address} ${(data[i].add).street} ${(data[i].add).city} </p>
                <br>
                
                <hr style="border-top: dotted 1px;" />
                <br>
            </div>
            `
        }
    }



    return cards;
}

const refundBtn = document.querySelector('.refundBtn');
/*
<button class="refundBtn">Refund</button>
refundBtn.addEventListener('click', () => {

})
*/
getRefunds();