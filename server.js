// importing packages


const express = require('express');
const admin = require('firebase-admin');
const bcrypt = require('bcrypt');
const path = require('path');
const nodemailer = require('nodemailer');

// firebase admin setup
let serviceAccount = require("./cs308-team16-firebase-adminsdk-s41mq-c0cf10fe5f");

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount)
});

let db = admin.firestore();

// aws config
const aws = require('aws-sdk');
const dotenv = require('dotenv');

dotenv.config();

// aws parameters
const region = "eu-central-1";
const bucketName = "cs308";
const accessKeyId = process.env.AWS_ACCESS_KEY;
const secretAccessKey = process.env.AWS_SECRET_KEY;

aws.config.update({
    region,
    accessKeyId,
    secretAccessKey
})

// init s3
const s3 = new aws.S3();

// generate image upload link
async function generateUrl() {
    let date = new Date();
    let id = parseInt(Math.random() * 10000000000);

    const imageName = `${id}${date.getTime()}.jpg`;

    const params = ({
        Bucket: bucketName,
        Key: imageName,
        Expires: 300, //300 ms
        ContentType: 'image/jpeg'
    })
    const uploadUrl = await s3.getSignedUrlPromise('putObject', params);
    return uploadUrl;
}

// declare static path
let staticPath = path.join(__dirname, "public");

//intializing express.js
const app = express();

//middlewares
app.use(express.static(staticPath));
app.use(express.json());

//routes
//home route
app.get("/", (req, res) => {
    res.sendFile(path.join(staticPath, "index.html"));
})

//signup route
app.get('/signup', (req, res) => {
    res.sendFile(path.join(staticPath, "signup.html"));
})

app.post('/signup', (req, res) => {
    let { name, email, password, number, tac, notification } = req.body;

    // form validations
    if (name.length < 3) {
        return res.json({ 'alert': 'name must be 3 letters long' });
    } else if (!email.length) {
        return res.json({ 'alert': 'enter your email' });
    } else if (password.length < 8) {
        return res.json({ 'alert': 'password should be 8 letters long' });
    } else if (!number.length) {
        return res.json({ 'alert': 'enter your phone number' });
    } else if (!Number(number) || number.length < 10) {
        return res.json({ 'alert': 'invalid number, please enter valid one' });
    } else if (!tac) {
        return res.json({ 'alert': 'you must agree to our terms and conditions' });
    }

    // store user in db
    db.collection('users').doc(email).get()
        .then(user => {
            if (user.exists) {
                return res.json({ 'alert': 'email already exists' });
            } else {
                // encrypt the password before storing it.
                bcrypt.genSalt(10, (err, salt) => {
                    bcrypt.hash(password, salt, (err, hash) => {
                        req.body.password = hash;
                        db.collection('users').doc(email).set(req.body)
                            .then(data => {
                                res.json({
                                    name: req.body.name,
                                    email: req.body.email,
                                    seller: req.body.seller,
                                })
                            })
                    })
                })
            }
        })
})

// login route
app.get('/login', (req, res) => {
    res.sendFile(path.join(staticPath, "login.html"));
})

app.post('/login', (req, res) => {
    let { email, password } = req.body;

    if (!email.length || !password.length) {
        return res.json({ 'alert': 'fill all the inputs' })
    }

    db.collection('users').doc(email).get()
        .then(user => {
            if (!user.exists) { // if email does not exists
                return res.json({ 'alert': 'log in email does not exists' })
            } else {
                bcrypt.compare(password, user.data().password, (err, result) => {
                    if (result) {
                        let data = user.data();
                        return res.json({
                            name: data.name,
                            email: data.email,
                            seller: data.seller,
                        })
                    } else {
                        return res.json({ 'alert': 'password is incorrect' });
                    }
                })
            }
        })
})

// seller route
app.get('/seller', (req, res) => {
    res.sendFile(path.join(staticPath, "seller.html"));
})

app.post('/seller', (req, res) => {
    let { name, about, address, number, tac, legit, email } = req.body;
    if (!name.length || !address.length || !about.length || number.length < 10 || !Number(number)) {
        return res.json({ 'alert': 'some inforamation(s) is/are invalid' });
    } else if (!tac || !legit) {
        return res.json({ 'alert': 'you must agree to our terms and conditions' })
    } else {
        // update users seller status here.
        db.collection('sellers').doc(email).set(req.body)
            .then(data => {
                db.collection('users').doc(email).update({
                    seller: true
                }).then(data => {
                    res.json(true);
                })
            })
    }
})

// add product
app.get('/add-product', (req, res) => {
    res.sendFile(path.join(staticPath, "addProduct.html"));
})


app.get('/add-product/:id', (req, res) => {
    res.sendFile(path.join(staticPath, "addProduct.html"));
})


// get the upload link
app.get('/s3url', (req, res) => {
    generateUrl().then(url => res.json(url));
})


// add product
app.post('/add-product', (req, res) => {
    let { name, shortDes, des, images, sizes, actualPrice, discount, sellPrice, stock, tags, tac, email, draft, id } = req.body;

    // validation
    if (!draft) {
        if (!name.length) {
            return res.json({ 'alert': 'enter product name' });
        } else if (shortDes.length > 100 || shortDes.length < 10) {
            return res.json({ 'alert': 'short description must be between 10 to 100 letters long' });
        } else if (!des.length) {
            return res.json({ 'alert': 'enter detail description about the product' });
        } else if (!images.length) { // image link array
            return res.json({ 'alert': 'upload atleast one product image' })
        } else if (!sizes.length) { // size array
            return res.json({ 'alert': 'select at least one size' });
        } else if (!actualPrice.length || !discount.length || !sellPrice.length) {
            return res.json({ 'alert': 'you must add pricings' });
        } else if (stock < 20) {
            return res.json({ 'alert': 'you should have at least 20 items in stock' });
        } else if (!tags.length) {
            return res.json({ 'alert': 'enter few tags to help ranking your product in search' });
        } else if (!tac) {
            return res.json({ 'alert': 'you must agree to our terms and conditions' });
        }
    }

    // add product
    let docName = id == undefined ? `${name.toLowerCase()}-${Math.floor(Math.random() * 5000)}` : id;
    db.collection('products').doc(docName).set(req.body)
        .then(data => {
            res.json({ 'product': name });
        })
        .catch(err => {
            return res.json({ 'alert': 'some error occured. Try again' });
        })
})

// get products
app.post('/get-products', (req, res) => {
    let { email, id, tag } = req.body;

    if (id) {
        docRef = db.collection('products').doc(id)
    } else if (tag) {
        docRef = db.collection('products').where('tags', 'array-contains', tag)
    } else {
        docRef = db.collection('products').where('email', '==', email)
    }

    docRef.get()
        .then(products => {
            if (products.empty) {
                return res.json('no products');
            }
            let productArr = [];
            if (id) {
                return res.json(products.data());
            } else {
                products.forEach(item => {
                    let data = item.data();
                    data.id = item.id;
                    productArr.push(data);
                })
                res.json(productArr)
            }
        })
})

// get products
app.post('/get-products-saler', (req, res) => {
    let { email, id, tag } = req.body;


    docRef = db.collection('products')
    

    docRef.get()
        .then(products => {
            if (products.empty) {
                return res.json('no products');
            }
            let productArr = [];
            if (id) {
                return res.json(products.data());
            } else {
                products.forEach(item => {
                    let data = item.data();
                    data.id = item.id;
                    productArr.push(data);
                })
                res.json(productArr)
            }
        })
})

app.post('/delete-product', (req, res) => {
    let { id } = req.body;

    db.collection('products').doc(id).delete()
        .then(data => {
            res.json('success');
        }).catch(err => {
            res.json('err');
        })
})

// product page
app.get('/products/:id', (req, res) => {
    res.sendFile(path.join(staticPath, "product.html"));
})

app.get('/search/:key', (req, res) => {
    res.sendFile(path.join(staticPath, "search.html"));
})

app.get('/cart', (req, res) => {
    res.sendFile(path.join(staticPath, "cart.html"));
})

app.get('/checkout', (req, res) => {
    res.sendFile(path.join(staticPath, "checkout.html"));
})

app.post('/order', (req, res) => {
    const { order, email, add } = req.body;

    let transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: process.env.EMAIL,
            pass: process.env.PASSWORD
        }
    })

    const mailOption = {
        from: 'sender email',
        to: email,
        subject: 'Clothing : Order Placed',
        html: `
        <!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="UTF-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <title>Document</title>

            <style>
                body{
                    min-height: 90vh;
                    background: #f5f5f5;
                    font-family: sans-serif;
                    display: flex;
                    justify-content: center;
                    align-items: center;
                }
                .heading{
                    text-align: center;
                    font-size: 40px;
                    width: 50%;
                    display: block;
                    line-height: 50px;
                    margin: 30px auto 60px;
                    text-transform: capitalize;
                }
                .heading span{
                    font-weight: 300;
                }
                .btn{
                    width: 200px;
                    height: 50px;
                    border-radius: 5px;
                    background: #3f3f3f;
                    color: #fff;
                    display: block;
                    margin: auto;
                    font-size: 18px;
                    text-transform: capitalize;
                }
            </style>

        </head>
        <body>
            
            <div>
                <h1 class="heading">dear ${email.split('@')[0]}, <span>your order is successfully placed</span></h1>
                <button class="btn">check status</button>
            </div>

        </body>
        </html>
        `
    }

    

    let docName = email + Math.floor(Math.random() * 123719287419824);
    db.collection('order').doc(docName).set(req.body)
        .then(data => {

            transporter.sendMail(mailOption, (err, info) => {
                if (err) {
                    res.json({ 'alert': 'opps! its seems like some err occured. Try again' })
                } else {
                    res.json({ 'alert': 'your order is placed', 'type': 'success' });
                }
            })

        })
})

//review routes
app.post('/add-review', (req, res) => {
    let = { headline, review, rate, email, product } = req.body;
    //form valid
    if (!headline.length || !review.length || rate == 0) {
        return res.json({ 'alert': 'Fill all the inputs' });
    }
    // storing in Firestore
    let docName = `review-${email}-${product}`; //let docName = `review-${email}-${product}`
    db.collection('reviews').doc(docName).set(req.body)
        .then(data => {
            res.json('review')
        })

    /*
            db.collection("cities").doc("LA").set({
    name: "Los Angeles",
    state: "CA",
    country: "USA"

        let reviews = collection(db, "reviews");

    
        let docName = `review-${email}`; //let docName = `review-${email}-${product}`;

        setDoc(doc(reviews, docName), req.body)
            .then(data => {
                return res.json('review')
            }).catch(err => {
                console.log(err)
                res.json({ 'alert': 'some err occured' })
            });
        */
})

app.post('/get-reviews', (req, res) => {
        let { email, product } = req.body;
        //var reviews = db.collection("reviews");
        console.log(req.body);

        (function(querySnapshot) {
            querySnapshot.forEach(function(doc) {
                // doc.data() is never undefined for query doc snapshots
                console.log(doc.id, " => ", doc.data());
            });
        })


        db.collection('reviews').where("product", "==", product).get()
            .then(function(querySnapshot) {
                //console.log(review);
                let = reviewArr = [];


                if (querySnapshot.empty) {
                    return res.json(reviewArr);
                }

                let userEmail = false;

                querySnapshot.forEach(function(doc) {
                    let reivewEmail = doc.data().email;
                    if (reivewEmail == email) {
                        userEmail = true;
                    }
                    reviewArr.push(doc.data())
                        //console.log(doc.product, " => ", doc.data());
                })


                if (!userEmail) {
                    db.collection('reviews').doc(`review-${email}-${product}`).get()
                        .then(data => reviewArr.push(data.data()))
                }

                return res.json(reviewArr);

            }).catch(function(error) {
                console.log("Error getting document:", error);
            });

    })
    //})

app.post('/get-reviews2', (req, res) => {
    let { email } = req.body;
    //var reviews = db.collection("reviews");
    //console.log(req.body);

    (function(querySnapshot) {
        querySnapshot.forEach(function(doc) {
            // doc.data() is never undefined for query doc snapshots
            console.log(doc.id, " => ", doc.data());
        });
    })


    db.collection('reviews').get()
        .then(function(querySnapshot) {
            //console.log(review);
            let = reviewArr = [];


            if (querySnapshot.empty) {
                return res.json(reviewArr);
            }

            querySnapshot.forEach(function(doc) {
                reviewArr.push(doc.data())
                    //console.log(doc.product, " => ", doc.data());
            })


            return res.json(reviewArr);

        }).catch(function(error) {
            console.log("Error getting document:", error);
        });

})


app.post('/get-stocks', (req, res) => {
    let { name, timebought } = req.body;

    db.collection("products").where("name", "==", name).get().then((querySnapshot) => {

        querySnapshot.forEach((doc) => {
            console.log(req.body);
            const stock_amount = querySnapshot.docs.map(doc => doc.data().stock);
            var intstock = parseInt(stock_amount, 10) - timebought;
            //console.log(stock_amount);
            db.collection('products').doc(doc.id).update({ stock: intstock });

        });

    }).catch(function(error) {
        console.log("Error getting document:", error);
    });


})

//review routes
app.post('/add-review', (req, res) => {
    let = { headline, review, rate, email, product } = req.body;
    //form valid
    if (!headline.length || !review.length || rate == 0) {
        return res.json({ 'alert': 'Fill all the inputs' });
    }
    // storing in Firestore
    let docName = `review-${email}-${product}`; //let docName = `review-${email}-${product}`
    db.collection('reviews').doc(docName).set(req.body)
        .then(data => {
            res.json('review')
        })

    /*
            db.collection("cities").doc("LA").set({
    name: "Los Angeles",
    state: "CA",
    country: "USA"

        let reviews = collection(db, "reviews");

    
        let docName = `review-${email}`; //let docName = `review-${email}-${product}`;

        setDoc(doc(reviews, docName), req.body)
            .then(data => {
                return res.json('review')
            }).catch(err => {
                console.log(err)
                res.json({ 'alert': 'some err occured' })
            });
        */
})

app.post('/get-refunds', (req, res) => {
    let { email } = req.body;
    //var reviews = db.collection("reviews");
    //console.log(req.body);

    (function(querySnapshot) {
        querySnapshot.forEach(function(doc) {
            // doc.data() is never undefined for query doc snapshots
            console.log(doc.id, " => ", doc.data());
        });
    })


    db.collection('order').where("email", "==", email).get()
        .then(function(querySnapshot) {
            let = reviewArr = [];


            if (querySnapshot.empty) {
                return res.json(reviewArr);
            }

            let userEmail = false;

            querySnapshot.forEach(function(doc) {
                let refundEmail = doc.data().email;
                if (refundEmail == email) {
                    userEmail = true;
                }
                db.collection('order').doc(doc.id).update({ orderID: doc.id })



                //console.log(doc.data() + doc.id)
                reviewArr.push(doc.data())
                    //console.log(doc.product, " => ", doc.data());
            })


            if (!userEmail) {
                db.collection('reviews').doc(`review-${email}-${product}`).get()
                    .then(data => reviewArr.push(data.data()))
            }

            return res.json(reviewArr);

        }).catch(function(error) {
            console.log("Error getting document:", error);
        });

})

app.post('/add-refund-request', (req, res) => {
    let = {
        email,
        orderID,
        productName,
        orderPrice,
        approvalRefundRequest,
        approvedOrDisapproved
    } = req.body;
    let docName = orderID;

    db.collection("order").where(admin.firestore.FieldPath.documentId(), '==', orderID).get().then((querySnapshot) => {

        querySnapshot.forEach((doc) => {
            let productName = (doc.data().order[0].name);
            let orderPrice = (doc.data().order[0].sellPrice);
            //console.log(productName)
            db.collection('refundRequests').doc(docName).set({
                productName: productName,
                orderPrice: orderPrice,
                orderID: docName,
                email: email,
                approvalRefundRequest: approvalRefundRequest,
                approvedOrDisapproved: approvedOrDisapproved,
            })


        });

    })


    // storing in Firestore
    //let docName = `refundreq-${email}-${refundID}`; 

    db.collection('refundRequests').doc(docName).set(req.body)
        .then(data => {
            res.json('refund')
        })
})

app.post('/get-refund-req', (req, res) => {
    let { email } = req.body;
    //var reviews = db.collection("reviews");
    //console.log(req.body);

    (function(querySnapshot) {
        querySnapshot.forEach(function(doc) {
            // doc.data() is never undefined for query doc snapshots
            //console.log(doc.id, " => ", doc.data());
        });
    })


    db.collection('refundRequests').get()
        .then(function(querySnapshot) {
            //console.log(review);
            let = reviewArr = [];


            if (querySnapshot.empty) {
                return res.json(reviewArr);
            }

            querySnapshot.forEach(function(doc) {
                reviewArr.push(doc.data())
                    //console.log(doc.product, " => ", doc.data());
            })


            return res.json(reviewArr);

        }).catch(function(error) {
            console.log("Error getting document:", error);
        });

})

app.post('/approve-refund-req', (req, res) => {
    let { orderID } = req.body;
    //var reviews = db.collection("reviews");
    //console.log(req.body);

    db.collection('refundRequests').doc(orderID).update({
        approvalRefundRequest: true,
        approvedOrDisapproved: "approved"
    })

    if (db.collection('orders').doc(orderID).exists) {
        db.collection('orders').doc(orderID).update({
            approvalRR: true,
            approvedOrDisapproved: "approved"
        })
    }




})

// 404 route
app.get('/404', (req, res) => {
    res.sendFile(path.join(staticPath, "404.html"));
})

app.use((req, res) => {
    res.redirect('/404');
})

app.listen(3000, () => {
    console.log('listening on port 3000.......');
})